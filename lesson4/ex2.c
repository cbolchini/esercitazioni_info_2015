#define TOP_N 5
#define PILOTI 22
#define NAME_LEN 25

int main(int argc, char* argv[]){
    char piloti[PILOTI][NAME_LEN+1];
    float tempi[PILOTI];
    int tagMigliori[PILOTI];
    int top[TOP_N];
    int i, j;
    int inserimento;

    for ( i = 0; i < PILOTI; i++ )
        scanf("%s %f",piloti[i],&tempi[i]);

    for ( i = 0; i < TOP_N; i++ )
        top[i]=-1;

    for ( i = 0; i < PILOTI; i++ ){
        inserimento = -1;
        for ( j = 0; j < TOP_N  && inserimento == -1 ; j++ ){
            if ( top[j] == -1 || tempi[top[j]] > tempi[i] ){
                inserimento=j;
            }
        }
        if ( inserimento != -1 ){
            /*sposto tutti a destra di 1*/
            for ( j = TOP_N-1 ; j > inserimento ; j-- ){
                top[j]=top[j-1];
            }
            top[inserimento] = i;
        }
    }

    for ( i = 0; i < PILOTI; i++ )
        tagMigliori[i] = 0;

    for ( i = 0; i < TOP_N; i++ )
        tagMigliori[top[i]]=1+i;

    for ( i = 0; i < PILOTI; i++ ){
        printf("%s %f",piloti[i],tempi[i]);
        if ( tagMigliori[i] != 0 ){
            printf(" BEST %d",tagMigliori[i]);
        }
        printf("n");
    }
}

