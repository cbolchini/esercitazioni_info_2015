#include <stdio.h>

int main(int argc, char *argv[])
{
    int a;
    int prec, succ;

    printf("Insert a number ") ;
    scanf("%d", &a);

    prec = a-1;
    succ = a+1;

    printf("\n");
    printf("The inserted number is %d\n", a);
    printf("The preceding number is %d\n", prec);
    printf("The next number is %d\n", succ);

    return 0;
}
