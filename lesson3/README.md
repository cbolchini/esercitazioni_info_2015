#ex. 1
Implementare la seguente variante del [http://it.wikipedia.org/wiki/Cifrario_di_Cesare](cifrario di cesare)
Acquisire una sequenza di caratteri maiuscoli e spazi di *al massimo* 160 caratteri, che si ritiene terminata dal carattere '*'.
Assumere che non ci siano errori nell'input.
Il programma dovrà chiedere all’utente la chiave sotto forma di numero intero compreso tra 1 e 10.

Stampare il messaggio cifrato ottenuto nel seguente modo:
leggere il messaggio al contrario (partendo quindi dall'ultimo carattere fino ad arrivare al primo)
traslando ogni lettera di K posizioni in avanti (dove K è la chiave) carattere cifrato = carattere plain + K.


#ex. 2
Acquisire una matrice di dimensione 2x2 e calcolarne il determinante

A=   | a  b |
     | c  d |

det(A) = ad-bc

#ex. 3
Scrivere un programma che verifichi se una matrice e' diagonale.
Il programma dovra':
- Acquisire il numero di righe e colonne.
- Acquisire gli elementi della matrice (riga per riga)
- Stampare se la matrice e' diagonale o meno


Il programma accetta matrici di dimensione massima 10x10.


#ex. 4
```
Un quadrato magico è uno schieramento di numeri interi distinti in una tabella quadrata
tale che la somma dei numeri presenti in ogni riga, in ogni colonna e in entrambe
le diagonali dia sempre lo stesso numero
```

Si scriva un programma che acquisisca un [quadrato magico](https://it.wikipedia.org/wiki/Quadrato_magico) di dimensione 3x3
E verifichi che questo sia un quadrato magico, in caso affermativo stampi la costante di magia (il valore della somma).


#ex. 5
Si scriva un programma che calcoli il prodotto tra due matrici di interi.
Il programma deve acquisire i sotto forma di intero il numero di righe e colonne della prima
acquisire quindi gli elementi della matrice (riga per riga),
acquisire il numero di colonne della seconda (verificare che il numero di righe sia uguale al
numero di colonne della prima!)
acquisire quindi i suoi elementi i suoi elementi.
Il programma deve stampare la matrice risultante.

Il programma accetta matrici di dimensione massima 10×10.


# homework 1
*In un programma separato* implementare la decifratura di un messaggio cifrato tramite il metodo
del cifrario di cesare cosi' come implementato nell'esercizio 1.

# homework 2
Implementare la seguente ottimizzazione all'esercizio 5:
- Verificare se entrambe le matrici sono triangolari superiori, o triangolari inferiori.
- calcolare il prodotto tra matrici calcolando in maniera diretta i valori sulla diagonale
- evitare di calcolare i prodotti nulli (si puo' inizializzare tutta la matrice risultato C a 0,
 o meglio inializzare a 0 solo la parte che non verra' considerata)

