#define LEN 160
#define KMIN 0
#define KMAX 10

#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
    char plain[LEN+1];
    char enc[LEN+1];

    int i;
    int key;
    int len;


    printf("Acquisizione messaggio: ");

    i = 0;

    do{
        scanf("%c", &plain[i]);
        i++;
    }while( i<LEN+1 && plain[i-1]!='*' );

    len = i-2;



    do {
        printf("Inserire la key di cifratura: intero in [%d, %d]: ",
               KMIN, KMAX);
        scanf("%d", &key);

        if (key < KMIN || key > KMAX)
            printf("Chiave non valida!");
    } while (key < KMIN || key > KMAX);


    printf("Messaggio cifrato: ");

    //cifratura (scorrimento al contrario + key)
    for (i = 0; i < len+1; i++)
        if(plain[len-i]==' '){
            enc[i] = ' ';
        } else {
            enc[i] = (plain[len-i]+key);
            if(enc[i]>'Z')
                enc[i] = enc[i]-'Z'+'A';
        }

    for(i = 0; i < len; i++)
        printf("%c", enc[i]);

    printf("\n\n");

    return 0;
}

