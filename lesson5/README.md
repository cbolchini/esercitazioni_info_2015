#lesson 5 - /11/2015

## ex. 1
Sviluppare un programma che svolge le seguenti operazioni:

  + Acquisisce informazioni relative ad una serie di rilievi altimetrici, **fino ad un massimo di 10 rilievi**.
      (i.e. richiedere all'utente se vuole inserire un dato o meno)
      Ogni rilievo è caratterizzato da una latitudine, una longitudine ed una altitudine.
  + Terminata la fase di acquisizione, stampa sullo schermo le informazioni relative a tutti i rilievi per
      i quali il valore della longitudine è pari


## ex. 2
Si sviluppi un programma che svolge le seguenti operazioni:

  + Acquisisce le informazioni relative a cinque studenti e le memorizza in un array.
  + Richiede all’utente di inserire il numero di matricola di uno studente di cui cercare le informazioni
      nell’array.
  + Effettua la ricerca e stampa i dati che eventualmente verranno trovati nell’array.


## ex. 3

